#! /bin/bash

if [[ $EUID -ne 0 ]]; then
	echo "You must be root!" 1>&2
	exit 1
fi

#Name of daemon jar file.
DAEMON=SettlementProcessor
# Directory where the application resides
DIR=$(pwd)
# Application JAR file (may be in a sub folder)
JAR_FILE=$DIR/dist/$DAEMON.jar:$DIR/dist/lib/*

# JVM in use
# JAVA_HOME=$(which java)

# Main class implementing the Daemon interface
MAIN_CLASS=com.cellulant.hub4.Crypto
# Logging properties file

MIN_MEMORY=-Xms256m
MAX_MEMORY=-Xmx512m

# Set to 1 to enable debugging
DEBUG=1
DEBUG_OUTPUT_FILE=$DIR/output.txt
DEBUG_ERROR_FILE=$DIR/error.txt


#Configs Folder location...
CONFIGLOCATION=$DIR

# DO NOT EDIT BELOW THIS LINE

usage() {
	echo $"Usage: $0 [-e <string> | -d <sting>]"
	return 0
}

while getopts ":e:d:" opt; do
  case $opt in
    e)
      echo "Encrypting the string: $OPTARG" >&2
    	cd $DIR
    	java -cp $JAR_FILE $MAIN_CLASS "0" "$OPTARG"
    	exit 0
      ;;
    d)
      echo "Decrypting the string: $OPTARG" >&2
    	cd $DIR
    	java -cp $JAR_FILE $MAIN_CLASS "1" "$OPTARG"
    	exit 0
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
    	case $OPTARG in
    		e)
    			echo "Please provide a string to be encoded." >&2
    			exit 1
    		;;
    		d)
    			echo "Please provide a string to be decoded." >&2
    			exit 1
    		;;
		esac
      ;;
  esac
done

if [ -z "${e}" ] || [ -z "${d}" ]; then
    usage
fi

exit $?

