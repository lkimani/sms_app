/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.argila.core.dto;

import java.sql.Timestamp;

/**
 * A settlementDto class.
 *
 * @author lewis.kimani
 */
public final class SettlementDto {

    /**
     * collectionLogId.
     */
    private long settlementLogId;
    /**
     * settlementID.
     */
    private long settlementID;
    /**
     * paymentID.
     */
    private long paymentID;
    /**
     * serviceID.
     */
    private long serviceID;
    /**
     * computedMerchantAmount.
     */
    private double computedMerchantAmount;
    /**
     * ownerClientID.
     */
    private long ownerClientID;
    /**
     * commissionModelID.
     */
    private long commissionModelID;
    /**
     * commissionSettingID.
     */
    private long commissionSettingID;
    /**
     * transactionCount.
     */
    private int transactionCount;
    /**
     * totalAmount.
     */
    private Double totalAmount;
    /**
     * ownerClient.
     */
    private String ownerClient;
    /**
     * service.
     */
    private String service;
    /**
     * settlementBankBranchID.
     */
    private String settlementBankBranchID;
    /**
     * settlement Bank Branch Name
     */
    private String settlementBankBranchName;
    /**
     * settlementAccountName.
     */
    private String settlementAccountName;
    /**
     * settlementAccountNumber.
     */
    private String settlementAccountNumber;
    /**
     * Settlement charge.
     */
    private Double settlementCharge;
    /**
     * autoSettle.
     */
    private int autoSettle;
    /**
     * trxStartDate.
     */
    private Timestamp trxStartDate;
    /**
     * trxEndDate.
     */
    private Timestamp trxEndDate;
    /**
     * countryCode.
     */
    private String countryCode;
    /**
     * computedCommissionAmount.
     */
    private double computedCommissionAmount;
    /**
     * currency.
     */
    private String currency;
    private String bankName;
    private String postalAddress;
    /**
     * branchCode.
     */
    private String branchCode;
    /**
     * indicates if data object is for reversal.
     */
    boolean isReveresal = false;
    private double reversedCommissions;
    private double totalReversedAmount;

    /**
     * @param sLogId settlementLogID
     * @param sId settlementID
     * @param sID serviceID
     * @param ownerID ownerClientID
     * @param commissionID commissionModelID
     * @param tCount transactionCount
     * @param tAmount totalAmount
     * @param owner ownerClient
     * @param svce service
     * @param settlementBranchID settlementBankBranchID
     * @param settlementBranchName
     * @param settlementAcntName settlementAccountName
     * @param settlementAccntNumber settlementAccountNumber
     * @param settlementCharge settlementCharge
     * @param autoSttle autoSettle
     * @param tStartDate trxStartDate
     * @param tEndDate trxEndDate
     * @param cCode countryCode
     * @param crncy currency
     * @param bName
     * @param pAddress
     * @param bCode branchCode
     * @param pID paymentID
     * @param reverse flag for reversal
     */
    public SettlementDto(final long sLogId, final long sId, final long sID,
            final long ownerID, final long commissionID,
            final int tCount, final Double tAmount, final String owner,
            final String svce, final String settlementBranchID, final String settlementBranchName,
            final String settlementAcntName, final String settlementAccntNumber,
            final double settlementCharge,
            final int autoSttle, final Timestamp tStartDate,
            final Timestamp tEndDate, final String cCode,
            final String crncy, final String bName, final String pAddress,
            final String bCode, final boolean reverse) {
        this.settlementLogId = sLogId;
        this.settlementID = sId;
        this.serviceID = sID;
        this.ownerClientID = ownerID;
        this.commissionModelID = commissionID;
        this.transactionCount = tCount;
        this.totalAmount = tAmount;
        this.ownerClient = owner;
        this.service = svce;
        this.settlementBankBranchID = settlementBranchID;
        this.settlementBankBranchName = settlementBranchName;
        this.settlementAccountName = settlementAcntName;
        this.settlementAccountNumber = settlementAccntNumber;
        this.settlementCharge = settlementCharge;
        this.autoSettle = autoSttle;
        this.trxStartDate = tStartDate;
        this.trxEndDate = tEndDate;
        this.countryCode = cCode;
        this.currency = crncy;
        this.bankName = bName;
        this.postalAddress = pAddress;
        this.branchCode = bCode;
        this.isReveresal = reverse;
    }

    public double getReversedCommissions() {
        return reversedCommissions;
    }

    public void setReversedCommissions(double reversedCommissions) {
        this.reversedCommissions = reversedCommissions;
    }

    public double getTotalReversedAmount() {
        return totalReversedAmount;
    }

    public void setTotalReversedAmount(double totalReversedAmount) {
        this.totalReversedAmount = totalReversedAmount;
    }

    public String getSettlementBankBranchName() {
        return settlementBankBranchName;
    }

    public void setSettlementBankBranchName(String settlementBankBranchName) {
        this.settlementBankBranchName = settlementBankBranchName;
    }

    public String getBankName() {
        return bankName;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public long getPaymentID() {
        return paymentID;
    }

    public void setPaymentID(long paymentID) {
        this.paymentID = paymentID;
    }

    public boolean isIsReveresal() {
        return isReveresal;
    }

    public void setIsReveresal(boolean isReveresal) {
        this.isReveresal = isReveresal;
    }

    public void setSettlementLogId(long settlementLogId) {
        this.settlementLogId = settlementLogId;
    }

    public long getSettlementLogId() {
        return settlementLogId;
    }

    /**
     * @return the settlementID
     */
    public long getSettlementID() {
        return settlementID;
    }

    /**
     * @param sID for settlementID
     */
    public void setSettlementID(final long sID) {
        this.settlementID = sID;
    }

    /**
     * @return the serviceID
     */
    public long getServiceID() {
        return serviceID;
    }

    /**
     * @param sId serviceId
     */
    public void setServiceID(final long sId) {
        this.serviceID = sId;
    }

    /**
     * @return the computedMerchantAmount
     */
    public double getComputedMerchantAmount() {
        return computedMerchantAmount;
    }

    public Double getSettlementCharge() {
        return settlementCharge;
    }

    public void setSettlementCharge(Double settlementCharge) {
        this.settlementCharge = settlementCharge;
    }

    /**
     * @param merchantAmount computedMerchantAmount
     */
    public void setComputedMerchantAmount(final double merchantAmount) {
        this.computedMerchantAmount = merchantAmount;
    }

    /**
     * @return the ownerClientID
     */
    public long getOwnerClientID() {
        return ownerClientID;
    }

    /**
     * @param ownerId ownerClientId
     */
    public void setOwnerClientID(final long ownerId) {
        this.ownerClientID = ownerId;
    }

    /**
     * @return the commissionModelID
     */
    public long getCommissionModelID() {
        return commissionModelID;
    }

    /**
     * @param commissionID commissionModelID
     */
    public void setCommissionModelID(final long commissionID) {
        this.commissionModelID = commissionID;
    }

    /**
     * @return the commissionModelID
     */
    public long getCommissionSettingID() {
        return commissionSettingID;
    }

    /**
     * @param comSettingID commissionSettingID
     */
    public void setCommissionSettingID(final long comSettingID) {
        this.commissionSettingID = comSettingID;
    }

    /**
     * @return the transactionCount
     */
    public int getTransactionCount() {
        return transactionCount;
    }

    /**
     * @param tCount transactionCount
     */
    public void setTransactionCount(final int tCount) {
        this.transactionCount = tCount;
    }

    /**
     * @return the totalAmount
     */
    public Double getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param ttlAmount totalAmount
     */
    public void setTotalAmount(final Double ttlAmount) {
        this.totalAmount = ttlAmount;
    }

    /**
     * @return the ownerClient
     */
    public String getOwnerClient() {
        return ownerClient;
    }

    /**
     * @param owner ownerClient
     */
    public void setOwnerClient(final String owner) {
        this.ownerClient = owner;
    }

    /**
     * @return the service
     */
    public String getService() {
        return service;
    }

    /**
     * @param servce service
     */
    public void setService(final String servce) {
        this.service = servce;
    }

    /**
     * @return the settlementBankBranchID
     */
    public String getSettlementBankBranchID() {
        return settlementBankBranchID;
    }

    /**
     * @param settlementBranchID settlementBankBranchID
     */
    public void setSettlementBankBranchID(final String settlementBranchID) {
        this.settlementBankBranchID = settlementBranchID;
    }

    /**
     * @return the settlementAccountName
     */
    public String getSettlementAccountName() {
        return settlementAccountName;
    }

    /**
     * @param accountName settlementAccountName
     */
    public void setSettlementAccountName(final String accountName) {
        this.settlementAccountName = accountName;
    }

    /**
     * @return the settlementAccountNumber
     */
    public String getSettlementAccountNumber() {
        return settlementAccountNumber;
    }

    /**
     * @param sAccountNumber settlementAccountNumber
     */
    public void setSettlementAccountNumber(final String sAccountNumber) {
        this.settlementAccountNumber = sAccountNumber;
    }

    /**
     * @return the autoSettle
     */
    public int isAutoSettle() {
        return autoSettle;
    }

    /**
     * @param autoSttle autoSettle
     */
    public void setAutoSettle(final int autoSttle) {
        this.autoSettle = autoSttle;
    }

    /**
     * @return the trxStartDate
     */
    public Timestamp getTrxStartDate() {
        return trxStartDate;
    }

    /**
     * @param tStartDate trxStartDate
     */
    public void setTrxStartDate(final Timestamp tStartDate) {
        this.trxStartDate = tStartDate;
    }

    /**
     * @return the trxEndDate
     */
    public Timestamp getTrxEndDate() {
        return trxEndDate;
    }

    /**
     * @param tEndDate trxEndDate
     */
    public void setTrxEndDate(final Timestamp tEndDate) {
        this.trxEndDate = tEndDate;
    }

    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param cCode countryCode
     */
    public void setCountryCode(final String cCode) {
        this.countryCode = cCode;
    }

    /**
     * @return the computedCommissionAmount
     */
    public double getComputedCommissionAmount() {
        return computedCommissionAmount;
    }

    /**
     * @param cCommissionAmount computedCommissionAmount
     */
    public void setComputedCommissionAmount(final double cCommissionAmount) {
        this.computedCommissionAmount = cCommissionAmount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currncy currency
     */
    public void setCurrency(final String currncy) {
        this.currency = currncy;
    }

    /**
     * @return the branchCode
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * @param bCode branchCode
     */
    public void setBranchCode(final String bCode) {
        this.branchCode = bCode;
    }

    @Override
    public String toString() {
        return "SettlementDto{" + "settlementLogId=" + settlementLogId
                + ", settlementID=" + settlementID + ", "
                + "serviceID=" + serviceID + ", computedMerchantAmount="
                + computedMerchantAmount + ", ownerClientId=" + ownerClientID
                + ", commissionModelID=" + commissionModelID + ", "
                + "commissionSettingID=" + commissionSettingID
                + ", transactionCount=" + transactionCount + ", totalAmount="
                + totalAmount + ", ownerClient=" + ownerClient
                + ", service=" + service + ", settlementBankBranchID="
                + settlementBankBranchID + ", settlementAccountName="
                + settlementAccountName + ", settlementAccountNumber="
                + settlementAccountNumber + ", autoSettle=" + autoSettle
                + ", trxStartDate=" + trxStartDate + ", trxEndDate="
                + trxEndDate
                + ", countryCode=" + countryCode + ", computedCommissionAmount="
                + computedCommissionAmount + ", currency=" + currency + ", "
                + "branchCode=" + branchCode + "isReversal=" + isReveresal + '}';
    }

}
