/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.argila.core.dto;

/**
 *
 * @author lewis
 */
public final class Settlements {

    private long settlementID;
    private long beneficiaryClientID;

    private final long settlementLogID;
    private final String settlementType;
    private final long serviceID;
    private final int commissionModelID;
    private final double totalAmount;
    private final double settledAmount;
    private final double computedCommissionAmount;
    private final int transactionCount;
    private final String currency;
    private final double settlementCharge;

    public Settlements(long bclient, long sLogID, String sType, long srID, int cSID,
            double tAmount, double sAmount, double cCAmount,
            int tCount, String curcy, double sCharge) {
        this.beneficiaryClientID = bclient;
        this.settlementLogID = sLogID;
        this.commissionModelID = cSID;
        this.serviceID = srID;
        this.settlementType = sType;
        this.settledAmount = sAmount;
        this.totalAmount = tAmount;
        this.computedCommissionAmount = cCAmount;
        this.transactionCount = tCount;
        this.currency = curcy;
        this.settlementCharge = sCharge;

    }

    public long getSettlementID() {
        return settlementID;
    }

    public long getBeneficiaryClientID() {
        return beneficiaryClientID;
    }

    public void setSettlementID(long settlementID) {
        this.settlementID = settlementID;
    }

    public long getSettlementLogID() {
        return settlementLogID;
    }

    public String getSettlementType() {
        return settlementType;
    }

    public long getServiceID() {
        return serviceID;
    }

    public int getCommissionModelID() {
        return commissionModelID;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public double getSettledAmount() {
        return settledAmount;
    }

    public double getComputedCommissionAmount() {
        return computedCommissionAmount;
    }

    public int getTransactionCount() {
        return transactionCount;
    }

    public String getCurrency() {
        return currency;
    }

    public double getSettlementCharge() {
        return settlementCharge;
    }

    @Override
    public String toString() {
        return "Settlements{" + "settlementID=" + settlementID + ", beneficiaryClientID=" + beneficiaryClientID + ", settlementLogID=" + settlementLogID + ", settlementType=" + settlementType + ", serviceID=" + serviceID + ", commissionModelID=" + commissionModelID + ", totalAmount=" + totalAmount + ", settledAmount=" + settledAmount + ", computedCommissionAmount=" + computedCommissionAmount + ", transactionCount=" + transactionCount + ", currency=" + currency + ", settlementCharge=" + settlementCharge + '}';
    }

}
