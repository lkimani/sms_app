package com.argila.core.dto;

/**
 * Created by amosl on 07/10/2015.
 */
public final class CommissionSetting {

    private final long clientID;

    private final double fixedChargeAmount;

    private final double percentageAmount;

    private final int minTransactionCount;

    private final int maxTransactionCount;

    private final double minTransactionAmount;

    private final double maxTransactionAmount;

    public CommissionSetting(long clientID, double fixedChargeAmount, double percentageAmount, int minTransactionCount,
            int maxTransactionCount, double minTransactionAmount, double maxTransactionAmount) {
        this.clientID = clientID;
        this.fixedChargeAmount = fixedChargeAmount;
        this.percentageAmount = percentageAmount;
        this.minTransactionCount = minTransactionCount;
        this.maxTransactionCount = maxTransactionCount;
        this.minTransactionAmount = minTransactionAmount;
        this.maxTransactionAmount = maxTransactionAmount;
    }

    public long getClientID() {
        return clientID;
    }

    public double getFixedChargeAmount() {
        return fixedChargeAmount;
    }

    public double getPercentageAmount() {
        return percentageAmount;
    }

    public int getMinTransactionCount() {
        return minTransactionCount;
    }

    public int getMaxTransactionCount() {
        return maxTransactionCount;
    }

    public double getMinTransactionAmount() {
        return minTransactionAmount;
    }

    public double getMaxTransactionAmount() {
        return maxTransactionAmount;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CommissionSetting [clientID=" + clientID + ", fixedChargeAmount=" + fixedChargeAmount
                + ", percentageAmount=" + percentageAmount + ", minTransactionCount=" + minTransactionCount
                + ", maxTransactionCount=" + maxTransactionCount + ", minTransactionAmount=" + minTransactionAmount
                + ", maxTransactionAmount=" + maxTransactionAmount + "]";
    }

}
