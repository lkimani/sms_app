/**
 *
 */
package com.argila.core.dto;

import java.sql.Timestamp;
import java.util.Date;

/**
 * A collectionDto class.
 *
 * @author wekesa
 */
public final class ServiceDto {

    /**
     * serviceID.
     */
    private final long serviceID;
    /**
     * settlementID
     */
    private int settlementID;
    /**
     * settlementLog ID
     */
    private int settlementLogID;
    /**
     * client ID
     */
    private long clientID;
    /**
     * settlementBankBranchID.
     */
    private int settlementBankBranchID;
    /**
     * settlementAccountName.
     */
    private final String settlementAccountName;
    /**
     * settlementAccountNumber.
     */
    private final String settlementAccountNumber;
    /**
     * settlementAccountNumber.
     */
    private final String ownerClient;
    /**
     * serviceName.
     */
    private final String serviceName;
    /**
     * autoSettle.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int autoSettle;
    /**
     * settlementStartDate.
     */
    private final String settlementStartDate;
    /**
     * countryCode.
     */
    private final String countryCode;
    /**
     * currency.
     */
    private final String currency;
    /**
     * serviceID.
     */
    private final double cellulantCharge;
    /**
     * branchCode.
     */
    private final String branchCode;
    /**
     * setlementDuration
     */
    private String setlementDuration;
    /**
     * settlementUnit
     */
    private String settlementUnit;
    /**
     * swiftCode
     */
    private String swiftCode;
    /**
     * reversal amount
     */
    private double reversalAmount;
    /**
     * settling bank swift code.
     */
    private final String settlingswiftCode;
    private String bankName;

    /**
     * @param sId serviceId
     * @param acName
     * @param acNumber
     * @param svce serviceName
     * @param auto autoSettle
     * @param startDate settlementStartDate
     * @param cCode countryCode
     * @param curncy currency
     * @param charge cellulantCharge
     * @param sUnit
     * @param sDuration
     * @param swift
     * @param owner
     * @param bbCode
     * @param cID
     * @param sbID settlementBankBranchID
     * @param sscode
     */
    public ServiceDto(final long sId, final String acName,
            final String acNumber, final String svce, final int auto,
            final String startDate,
            final String cCode, final String curncy, final double charge,
            final String sUnit, final String sDuration, final String swift,
            final String owner, final String bbCode, final long cID,
            final int sbID, final String sscode, final String bname) {
        serviceID = sId;
        this.settlementAccountName = acName;
        this.settlementAccountNumber = acNumber;
        this.serviceName = svce;
        this.autoSettle = auto;
        this.settlementStartDate = startDate;
        this.countryCode = cCode;
        this.currency = curncy;
        this.cellulantCharge = charge;
        this.settlementUnit = sUnit;
        this.setlementDuration = sDuration;
        this.swiftCode = swift;
        this.ownerClient = owner;
        this.branchCode = bbCode;
        this.clientID = cID;
        this.settlementBankBranchID = sbID;
        this.settlingswiftCode = sscode;
        this.bankName = bname;
    }

    /**
     * @return the serviceID
     */
    public long getServiceID() {
        return serviceID;
    }

    public int getSettlementID() {
        return settlementID;
    }

    public String getOwnerClient() {
        return ownerClient;
    }

    public String getSettlingswiftCode() {
        return settlingswiftCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setSettlementID(int settlementID) {
        this.settlementID = settlementID;
    }

    public int getSettlementLogID() {
        return settlementLogID;
    }

    public void setSettlementLogID(int settlementLogID) {
        this.settlementLogID = settlementLogID;
    }

    public long getClientID() {
        return clientID;
    }

    public void setClientID(long clientID) {
        this.clientID = clientID;
    }

    public int getAutoSettle() {
        return autoSettle;
    }

    public double getReversalAmount() {
        return reversalAmount;
    }

    public void setReversalAmount(double reversalAmount) {
        this.reversalAmount = reversalAmount;
    }

    public void setAutoSettle(int autoSettle) {
        this.autoSettle = autoSettle;
    }

    public String getSetlementDuration() {
        return setlementDuration;
    }

    public void setSetlementDuration(String setlementDuration) {
        this.setlementDuration = setlementDuration;
    }

    public String getSettlementUnit() {
        return settlementUnit;
    }

    public void setSettlementUnit(String settlementUnit) {
        this.settlementUnit = settlementUnit;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    /**
     * @return the settlementAccountName
     */
    public String getSettlementAccountName() {
        return settlementAccountName;
    }

    /**
     * @return the settlementAccountNumber
     */
    public String getSettlementAccountNumber() {
        return settlementAccountNumber;
    }

    /**
     * @return the serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @return the autoSettle
     */
    public int autoSettle() {
        return autoSettle;
    }

    /**
     * @return the settlementStartDate
     */
    public String getSettlementStartDate() {
        return settlementStartDate;
    }

    /**
     * @return the settlementBankBranchID
     */
    public int getSettlementBankBranchID() {
        return settlementBankBranchID;
    }

    /**
     * @param cID the settlementBankBranchID to set.
     */
    public void setSettlementBankBranchID(final int cID) {
        this.settlementBankBranchID = cID;
    }

    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @return currency the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @return cellulantCharge the cellulantCharge
     */
    public double getCellulantCharge() {
        return cellulantCharge;
    }

    /**
     * @return branchCode the branchCode
     */
    public String getBranchCode() {
        return branchCode;
    }

    @Override
    public String toString() {
        return "ServiceDto{" + "serviceID=" + serviceID + ", settlementID=" + settlementID + ", settlementLogID=" + settlementLogID + ", clientID=" + clientID + ", settlementBankBranchID=" + settlementBankBranchID + ", settlementAccountName=" + settlementAccountName + ", settlementAccountNumber=" + settlementAccountNumber + ", ownerClient=" + ownerClient + ", serviceName=" + serviceName + ", autoSettle=" + autoSettle + ", settlementStartDate=" + settlementStartDate + ", countryCode=" + countryCode + ", currency=" + currency + ", cellulantCharge=" + cellulantCharge + ", branchCode=" + branchCode + ", setlementDuration=" + setlementDuration + ", settlementUnit=" + settlementUnit + ", swiftCode=" + swiftCode + ", reversalAmount=" + reversalAmount + '}';
    }

}
