/**
 *
 */
package com.argila.core.dto;

/**
 * @author amosl
 *
 */
public enum CommissionMode {
    /**
     * Fixed commission.
     */
    FIXED,
    /**
     * FIXED_PERCENTAGE commission.
     */
    FIXED_PERCENTAGE,
    /**
     * TIERED_AMOUNT_FIXED commission.
     */
    TIERED_AMOUNT_FIXED,
    /**
     * TIERED_COUNT_FIXED commission.
     */
    TIERED_COUNT_FIXED,
    /**
     * TIERED_AMOUNT_PERCENTAGE commission.
     */
    TIERED_AMOUNT_PERCENTAGE,
    /**
     * TIERED_COUNT_PERCENTAGE commission.
     */
    TIERED_COUNT_PERCENTAGE
}
