/**
 *
 */
package com.argila.core.dto;

import java.sql.Timestamp;

/**
 * @author lewie
 *
 */
public class BeneficiariesDto {

    private final int commissionModelID;

    private final long settlementLogId;
    private final long ownerClientID;
    /**
     * settlementBankBranchID.
     */
    private final int settlementBankBranchID;
    /**
     * settlement Bank Branch Name
     */
    private final String settlementBankBranchName;
    /**
     * settlementAccountName.
     */
    private final String settlementAccountName;
    /**
     * settlementAccountNumber.
     */
    private final String settlementAccountNumber;
    /**
     * Settlement charge.
     */
    private final Double settlementCharge;

    /**
     * countryCode.
     */
    private final String countryCode;
    /**
     * computedCommissionAmount.
     */
    private double computedCommissionAmount;
    /**
     * currency.
     */
    private String currency;
    private final String bankName;
    private final String postalAddress;
    /**
     * branchCode.
     */
    private final String branchCode;
    /**
     * transactionCount.
     */
    private int transactionCount;
    /**
     * totalAmount.
     */
    private Double totalAmount = 0.0;
    /**
     * totalAmount.
     */
    private Double totalCommissions;
    /**
     * ownerClient.
     */
    private final String ownerClient;
    /**
     * service name
     */
    private String service;
    /**
     * service ID
     */
    private Long serviceID;
    /**
     * trxEndDate.
     */
    private Timestamp trxEndDate;
    /**
     * settlementID.
     */
    private long settlementID;
    private double settledAmount;
    private int commissionModeID;

    /**
     * Constructor.
     *
     * @param slogID
     * @param clientID
     * @param cModelID
     * @param settlementBranchID
     * @param settlementBranchName
     * @param bCode
     * @param settlementAcntName
     * @param settlementAccntNumber
     * @param owner
     * @param crncy
     * @param settlementCharge
     * @param cCode
     * @param bName
     * @param pAddress
     */
    public BeneficiariesDto(final long slogID, final long clientID, final int cModelID,
            final int settlementBranchID, final String settlementBranchName,
            final String settlementAcntName, final String settlementAccntNumber,
            final double settlementCharge, final String cCode,
            final String crncy, final String bName, final String pAddress,
            final String bCode, final int cmid, final String owner) {
        this.settlementLogId = slogID;
        this.ownerClientID = clientID;
        this.commissionModelID = cModelID;
        this.settlementBankBranchID = settlementBranchID;
        this.settlementBankBranchName = settlementBranchName;
        this.settlementAccountName = settlementAcntName;
        this.settlementAccountNumber = settlementAccntNumber;
        this.settlementCharge = settlementCharge;
        this.countryCode = cCode;
        this.currency = crncy;
        this.bankName = bName;
        this.postalAddress = pAddress;
        this.branchCode = bCode;
        this.commissionModeID = cmid;
        this.ownerClient = owner;

    }

    public double getSettledAmount() {
        return settledAmount;
    }

    public int getCommissionModeID() {
        return commissionModeID;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setSettledAmount(double settledAmount) {
        this.settledAmount = settledAmount;
    }

    public Timestamp getTrxEndDate() {
        return trxEndDate;
    }

    public String getService() {
        return service;
    }

    public Long getServiceID() {
        return serviceID;
    }

    public void setServiceID(Long serviceID) {
        this.serviceID = serviceID;
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setComputedCommissionAmount(double computedCommissionAmount) {
        this.computedCommissionAmount = computedCommissionAmount;
    }

    public void setTrxEndDate(Timestamp trxEndDate) {
        this.trxEndDate = trxEndDate;
    }

    public long getSettlementID() {
        return settlementID;
    }

    public void setSettlementID(long settlementID) {
        this.settlementID = settlementID;
    }

    public long getSettlementLogId() {
        return settlementLogId;
    }

    public int getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(int transactionCount) {
        this.transactionCount = transactionCount;
    }

    public Double getTotalCommissions() {
        return totalCommissions;
    }

    public void setTotalCommissions(Double totalCommissions) {
        this.totalCommissions = totalCommissions;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getCommissionModelID() {
        return commissionModelID;
    }

    public long getOwnerClientID() {
        return ownerClientID;
    }

    public int getSettlementBankBranchID() {
        return settlementBankBranchID;
    }

    public String getSettlementBankBranchName() {
        return settlementBankBranchName;
    }

    public String getSettlementAccountName() {
        return settlementAccountName;
    }

    public String getSettlementAccountNumber() {
        return settlementAccountNumber;
    }

    public Double getSettlementCharge() {
        return settlementCharge;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public double getComputedCommissionAmount() {
        return computedCommissionAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getBankName() {
        return bankName;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public String getOwnerClient() {
        return ownerClient;
    }

    @Override
    public String toString() {
        return "BeneficiariesDto{" + "commissionModelID=" + commissionModelID + ", settlementLogId=" + settlementLogId + ", ownerClientID=" + ownerClientID + ", settlementBankBranchID=" + settlementBankBranchID + ", settlementBankBranchName=" + settlementBankBranchName + ", settlementAccountName=" + settlementAccountName + ", settlementAccountNumber=" + settlementAccountNumber + ", settlementCharge=" + settlementCharge + ", countryCode=" + countryCode + ", computedCommissionAmount=" + computedCommissionAmount + ", currency=" + currency + ", bankName=" + bankName + ", postalAddress=" + postalAddress + ", branchCode=" + branchCode + ", transactionCount=" + transactionCount + ", totalAmount=" + totalAmount + ", totalCommissions=" + totalCommissions + ", ownerClient=" + ownerClient + ", service=" + service + ", serviceID=" + serviceID + ", trxEndDate=" + trxEndDate + ", settlementID=" + settlementID + ", settledAmount=" + settledAmount + ", commissionModeID=" + commissionModeID + '}';
    }

}
