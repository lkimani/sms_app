/**
 *
 */
package com.argila.core.dto;

import java.util.Date;

/**
 * @author sunz
 *
 */
public class SummaryDto {

    private final long settlementID;
    private final int transactionCount;
    private final long merchantClientID;
    private final long serviceID;
    private final long payerClientID;
    private final String merchant;
    private final String service;
    private final String payer;
    private final String paymentMode;
    private final Date settlementDate;
    private final double totalAmount;
    private final double merchantEarnings;
    private final double cellulantCharge;
    private final double bankCharge;
    private final double cellulantCommission;
    private final double payerCommission;
    private final double acquirerCommission;

    /**
     *
     * @param settlementID
     * @param transactionCount
     * @param merchantClientID
     * @param serviceID
     * @param payerClientID
     * @param merchant
     * @param service
     * @param payer
     * @param paymentMode
     * @param settlementDate
     * @param totalAmount
     * @param merchantEarnings
     * @param cellulantCharge
     * @param bankCharge
     * @param cellulantCommission
     * @param payerCommission
     * @param acquirerCommission
     */
    public SummaryDto(long settlementID, int transactionCount,
            long merchantClientID, long serviceID, long payerClientID,
            String merchant, String service, String payer, String paymentMode,
            Date settlementDate, double totalAmount, double merchantEarnings,
            double cellulantCharge, double bankCharge,
            double cellulantCommission, double payerCommission,
            double acquirerCommission) {
        this.settlementID = settlementID;
        this.transactionCount = transactionCount;
        this.merchantClientID = merchantClientID;
        this.serviceID = serviceID;
        this.payerClientID = payerClientID;
        this.merchant = merchant;
        this.service = service;
        this.payer = payer;
        this.paymentMode = paymentMode;
        this.settlementDate = settlementDate;
        this.totalAmount = totalAmount;
        this.merchantEarnings = merchantEarnings;
        this.cellulantCharge = cellulantCharge;
        this.bankCharge = bankCharge;
        this.cellulantCommission = cellulantCommission;
        this.payerCommission = payerCommission;
        this.acquirerCommission = acquirerCommission;
    }

    /**
     * @return the settlementID
     */
    public long getSettlementID() {
        return settlementID;
    }

    /**
     * @return the transactionCount
     */
    public int getTransactionCount() {
        return transactionCount;
    }

    /**
     * @return the merchantClientID
     */
    public long getMerchantClientID() {
        return merchantClientID;
    }

    /**
     * @return the serviceID
     */
    public long getServiceID() {
        return serviceID;
    }

    /**
     * @return the payerClientID
     */
    public long getPayerClientID() {
        return payerClientID;
    }

    /**
     * @return the merchant
     */
    public String getMerchant() {
        return merchant;
    }

    /**
     * @return the service
     */
    public String getService() {
        return service;
    }

    /**
     * @return the payer
     */
    public String getPayer() {
        return payer;
    }

    /**
     * @return the paymentMode
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     * @return the settlementDate
     */
    public Date getSettlementDate() {
        return settlementDate;
    }

    /**
     * @return the totalAmount
     */
    public double getTotalAmount() {
        return totalAmount;
    }

    /**
     * @return the merchantEarnings
     */
    public double getMerchantEarnings() {
        return merchantEarnings;
    }

    /**
     * @return the cellulantCharge
     */
    public double getCellulantCharge() {
        return cellulantCharge;
    }

    /**
     * @return the bankCharge
     */
    public double getBankCharge() {
        return bankCharge;
    }

    /**
     * @return the cellulantCommission
     */
    public double getCellulantCommission() {
        return cellulantCommission;
    }

    /**
     * @return the payerCommission
     */
    public double getPayerCommission() {
        return payerCommission;
    }

    /**
     * @return the acquirerCommission
     */
    public double getAcquirerCommission() {
        return acquirerCommission;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SummaryDto [transactionCount=" + transactionCount
                + ", merchant=" + merchant + ", service=" + service
                + ", payer=" + payer + ", source=" + paymentMode
                + ", settlementDate=" + settlementDate + ", totalAmount="
                + totalAmount + ", merchantEarnings=" + merchantEarnings
                + ", cellulantCharge=" + cellulantCharge + ", bankCharge="
                + bankCharge + ", cellulantCommission=" + cellulantCommission
                + ", payerCommission=" + payerCommission
                + ", acquirerCommission=" + acquirerCommission + "]";
    }

}
