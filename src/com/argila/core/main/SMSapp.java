package com.argila.core.main;

import com.argila.core.SMSProcessor;
import com.argila.core.db.MySQL;
import com.argila.core.utils.CryptoUtil;
import com.argila.core.utils.Logger;
import com.argila.core.utils.Props;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.SQLException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author victor
 */
@SuppressWarnings({"ClassWithoutLogger", "FinalClass"})
public final class SMSapp {

    /**
     * Logger utility for this application.
     */
    private static Logger logger;

    /**
     * Initializes the MySQL connection pool.
     */
    private static MySQL mySQL;

    /**
     * The properties object.
     */
    private static Props props;

    /**
     * Encryption utility.
     */
    private static CryptoUtil cryptoUtil;

    /**
     * @param args the command line arguments
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public static void main(final String[] args) {
        try {
            init();
        } catch (Exception ex) {
            logger.error(ex.getLocalizedMessage(), ex);
            System.exit(1);
        }
        Thread t = new Thread(new SMSProcessor(props, mySQL));
        t.start();
    }

    /**
     * This function initializes all the components (MySQL, Properties, Logging
     * and Crypto utility) required for this application.
     *
     * @throws java.lang.Exception when something goes wrong
     */
    private static void init() throws Exception {
        props = new Props();

        if (!props.getLoadErrors().isEmpty()) {
            System.err.println("Error while loading properties:");

            for (String s : props.getLoadErrors()) {
                System.err.println(s);
            }

            System.err.println("Error while loading properties, "
                    + "system will exit with exit code 1.");
            System.exit(1);
        }

        logger = new Logger(SMSapp.class, props);

        logger.info(":: Initializing  SMS  Processor Application :: ");

        try {
//            cryptoUtil = new CryptoUtil(props);

//            String password = cryptoUtil.decrypt(props.getDbPassword());
            mySQL = new MySQL(props.getDbHost(), props.getDbPort(),
                    props.getDbName(), props.getDbUserName(), props.getDbPassword(),
                    props.getDbPoolName(), props.getDbPoolSize());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException ex) {
            System.err.println("Error occured while creating an "
                    + "encryption key, system will exit with exit code 1. "
                    + "Exception : " + ex.getMessage());
            System.exit(1);
        }
    }
}
