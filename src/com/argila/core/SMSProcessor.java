package com.argila.core;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.argila.core.db.MySQL;
import com.argila.core.dto.BeneficiariesDto;
import com.argila.core.dto.ServiceDto;
import com.argila.core.utils.Props;
import com.argila.core.utils.Constants;
import com.argila.core.utils.CoreUtils;
import com.argila.core.utils.DbUtil;
import com.argila.core.utils.Logger;
import com.argila.core.utils.MailUtil;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * SMSProcessor class.
 *
 * @author Lewis Kimani.
 */
public class SMSProcessor implements Runnable {

    /**
     * The properties object.
     */
    private final Props props;
    /**
     * Initializes the MySQL connection pool.
     */
    private final MySQL mySQL;
    /**
     * Logger utility for this application.
     */
    private final Logger logger;
    /**
     * Settlement process identifier in a list.
     */
    private List<Long> settlementLogIDs;
    /**
     * Settlement process identifier.
     */
    private Long settlementLogID;
    /**
     * Database utility object.
     */
    private final DbUtil dbUtil;
    /**
     * The string to append before the string being logged.
     */
    private final String logPreString;
    /**
     * mailer utility object.
     */
    private final MailUtil mailUtil;

    /**
     * SMSProcessor Constructor.
     *
     * @param prop object instance
     * @param mysql object instance
     */
    @SuppressWarnings("CallToThreadStartDuringObjectConstruction")
    public SMSProcessor(final Props prop, final MySQL mysql) {
        this.props = prop;
        this.logger = new Logger(getClass(), props);
        this.mySQL = mysql;
        this.dbUtil = new DbUtil(this.props, this.mySQL);
        this.mailUtil = new MailUtil(this.props, this.mySQL);
        this.logPreString = "SMSProcessor | ";

    }

    /**
     * Run method of this runnable class.
     */
    @Override
    public final void run() {
        String preLogString = this.logPreString + "run() | ";
        this.logger.info("The application has entered " + preLogString
                + "Method. It calls several other methods here- <<<");
        List<Map> accounts = fetchExpiredAccounts();
        if (insertSMS(accounts)) {
            this.logger.info(preLogString + "Successfully finished SMS job");
        } else {
            this.logger.error(preLogString + "finished SMS job with an error");
        }

        this.logger.info(preLogString + "Exit run: ->>> " + accounts.size());
    }

    /**
     * Insert an entry into the s_settlementLogs table. This entry will contain
     * information regarding this settlement process.
     *
     * @return <code>Long</code> Last insert ID.
     * @throws SQLException
     */
    private boolean insertSMS(List<Map> accounts) {
        String preLogString = this.logPreString + "insertSMS() | ";
        this.logger.info(preLogString + "Enter insertSMS...");
        String message = props.getInactiveSMSTemplate();
        for (Map account : accounts) {
            Set params = account.keySet();
            for (Object Oparam : params) {

                String param = String.valueOf(Oparam);
                String paramValue = String.valueOf(account.get(param));
                message = message.replace("^".concat(param).concat("^"), paramValue);
            }
            this.logger.info(preLogString
                    + " final message: "
                    + message);

            String insertQuery = "INSERT INTO sms_requests (destination, "
                    + "message, statusDesription,templateID,dateCreated ) "
                    + "VALUES (?,?,?,?,?)";
            try (Connection conn = this.mySQL.getConnection();
                    PreparedStatement stmt = conn.prepareStatement(insertQuery)) {
                stmt.setString(1, String.valueOf(account.get("MSISDN")));
                stmt.setString(2, message);
                stmt.setString(3, "Inactive account");
                stmt.setInt(4, 1);
                stmt.setTimestamp(5, new java.sql.Timestamp(
                        new java.util.Date().getTime()));

                stmt.executeUpdate();

            } catch (SQLException sqle) {
                this.logger.error(preLogString + "insertSMS  - " + sqle);
                return false;
            }
        }
        this.logger.info(preLogString + "Exit insertSMS... ");
        return true;
    }

    /**
     * This function gets the total cellulantCharge got earlier during
     * collections for the purpose of report generation.
     *
     * @return Total accounts .
     */
    private List< Map> fetchExpiredAccounts() {
        String preLogString = this.logPreString + " fetchExpiredAccounts() | ";
        this.logger.info(preLogString + "Enter fetchExpiredAccounts  ");

        List<Map> response = new ArrayList<>();
        String query = "SELECT `accountNumber`, `customer_accounts`.`customerProfileID` ,   "
                + "`MSISDN`, `customerName`,  `expiryDate`, `balanceCarriedForward`   "
                + "  FROM `customer_accounts`  "
                + " INNER JOIN `customerProfiles` "
                + " ON `customer_accounts`.`customerProfileID` = `customerProfiles`.`customerProfileID` "
                + " WHERE   expiryDate < now() ";

        this.logger.debug("Query: " + query);
        try (Connection conn = this.mySQL.getConnection();
                PreparedStatement stmt = conn.prepareStatement(query)) {

            try (ResultSet rs = stmt.executeQuery()) {

                int size = 0;
                if (rs.last()) {
                    size = rs.getRow();
                    rs.beforeFirst();
                }
                if (size > 0) {
                    this.logger.info(preLogString + " - ResultSet of size: "
                            + size);
                    while (rs.next()) {
                        Map<String, Object> account = new HashMap<>();
                        account.put("MSISDN", rs.getString("MSISDN"));
                        account.put("ACCOUNTNUMBER", rs.getString("accountNumber"));
                        account.put("CUSTOMERNAME", rs.getString("customerName"));
                        account.put("balanceCarriedForward", rs.getDouble("balanceCarriedForward"));
                        response.add(account);
                    }

                } else {
                    this.logger.info(preLogString
                            + " Empty result set.");
                }
            }
        } catch (SQLException e) {
            this.logger.error(preLogString + e.getLocalizedMessage(), e);
            return null;
        }

        return response;
    }

    /**
     * A method to rollback to check whether.
     *
     * @return list of failed queries
     */
    @SuppressWarnings("SleepWhileInLoop")
    private boolean rollbackSystem() {
        String preLogString = this.logPreString + "rollbackSystem() | ";
        this.logger.info(preLogString + "Enter rollbackSystem to check for any "
                + "failed queries- <<<");

        List<String> failedQueries
                = checkForFailedQueries("FAILED_QUERIES.TXT");

        if (failedQueries.size() <= 0) {
            this.logger.info(preLogString + "rollbackSystem: "
                    + "No failed queries found! Exit " + preLogString);
            return true;
        }
        // Failed queries were found on file!
        this.logger.info(preLogString + "rollbackSystem: "
                + failedQueries.size() + " failed queries found!");

        for (String reconQuery : failedQueries) {
            doRecon(reconQuery, 1);
            try {
                Thread.sleep(this.props.getSleepTimeMainThread());
            } catch (InterruptedException ex) {
                this.logger.error(preLogString + ex.getMessage(), ex);
            }
        }

        boolean rolledBack = checkForFailedQueries("FAILED_QUERIES.TXT").
                isEmpty();

        this.logger.info(preLogString + "I have finished performing "
                + "rollback...");
        this.logger.info(preLogString + "Exit rollbackSystem - >>> returning: "
                + rolledBack);

        return rolledBack;
    }

    /**
     * A method to check for failed queries.
     *
     * @return a list of failed queries
     * @param file to be read from
     */
    private List<String> checkForFailedQueries(final String file) {
        String preLogString = this.logPreString + "checkForFailedQueries() | ";
        this.logger.info(preLogString + "Enter function - <<<");

        List<String> queries = new ArrayList<>(0);
        if (new File(file).exists()) {
            try (FileInputStream fin = new FileInputStream(file);
                    DataInputStream in = new DataInputStream(fin);
                    BufferedReader br
                    = new BufferedReader(new InputStreamReader(in))) {

                String data;
                while ((data = br.readLine()) != null) {
                    queries.add(data);
                }

                if (queries.size() > 0) {
                    PrintWriter w = new PrintWriter(new FileWriter(file));
                    w.close();
                }

            } catch (IOException e) {
                this.logger.error(preLogString + e.getMessage(), e);
                // this is dangerous
                // we might end running the same settlement
            }
        }

        this.logger.info(preLogString + "Exit checkForFailedQueries ->>>, "
                + "size of queries list: " + queries.size());
        return queries;
    }

    /**
     * This method do reconciliation.
     *
     * @param query holds a query to be run
     * @param tries indicates that maximum number of retry
     */
    private void doRecon(final String query, final int tries) {
        String preLogString = this.logPreString + "doRecon() | ";
        this.logger.info(preLogString + "Enter doRecon - <<<");

        int maxRetry = this.props.getMaxSendRetries();
        if (!(query.toLowerCase().startsWith("update"))
                && !(query.toLowerCase().startsWith("insert"))) {
            this.logger
                    .info(preLogString + "doRecon: SQL [" + query + "] "
                            + "Could not be executed because it does not start "
                            + "with INSERT or UPDATE");
            return;
        }
        int qstate = this.dbUtil.updateRecord(query, new ArrayList<>());

        if (qstate != Constants.UPDATE_RECON_FAILED) {
            this.logger.info(preLogString + "Exit doRecon - >>>, qstate: "
                    + qstate);
            return;
        }

        this.logger.info(preLogString + "Failed to re-execute failed query: "
                + query + " [ Try " + tries + " out of  " + maxRetry);

        if (tries >= maxRetry) {
            this.logger.info(preLogString + "Exit doRecon - >>>, "
                    + "max re-tries exhausted!");
            return;
        }

        this.logger.info(preLogString + "Retrying in "
                + this.props.getSleepTime() / 1000 + " sec(s) ");

        try {
            Thread.sleep(this.props.getSleepTimeMainThread());
        } catch (InterruptedException ex) {
            this.logger.error(preLogString + ex.getMessage(), ex);
        }

        doRecon(query, tries + 1);
        this.logger.info(preLogString + "Exit doRecon - >>>, tries: " + tries);
    }

}
