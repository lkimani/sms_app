package com.argila.core.crypto;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.NoSuchPaddingException;

import com.argila.core.utils.CryptoUtil;
import com.argila.core.utils.Logger;
import com.argila.core.utils.Props;

/**
 * @author sunz
 *
 */
public class Crypto {
    /**
     * The CryptoUtil object.
     */
    private CryptoUtil encryptionUtil;
    /**
     * The properties object.
     */
    private Props props;
    /**
     * The properties object.
     */
    private Logger logger;
    /**
     * The Crypto Constructor.
     */
    public Crypto() {
        this.props = new Props();
        this.logger = new Logger(getClass(), this.props);

        try {
            this.encryptionUtil = new CryptoUtil(this.props);
        } catch (NoSuchAlgorithmException
                | NoSuchProviderException
                | NoSuchPaddingException e) {
            this.logger.error(this.getClass().getSimpleName() + ": "
                    + e.getMessage());
        }
    }
    /**
     * Method encrypt.
     * @return encodedText returned
     * @param  plainText to be encrypted
     */
    private String encrypt(final String plainText) {
        String encodedText = "";
        try {
            encodedText = this.encryptionUtil.encrypt(plainText);
        } catch (Exception e) {
            this.logger.error(this.getClass().getSimpleName() + ": "
                    + e.getMessage());
        }
        return encodedText;
    }
    /**
     * Method encrypt.
     * @return plainText returned
     * @param  encodedText to be encrypted
     */
    private String decode(final String encodedText) {
        String plainText = "";

        try {
            plainText = this.encryptionUtil.decrypt(encodedText);
        } catch (Exception e) {
            this.logger.error(this.getClass().getSimpleName() + ": "
                    + e.getMessage());
        }

        return plainText;
    }

    /**
     * @param args argument received
     */
    public static void main(final String[] args) {

        if (args.length < 2) {
            System.out.println("Args length: " + args.length);
            System.out.println("Required data missing, please ensure you "
                    + "provide a flag, either an 'e' or a 'd' AND a string "
                    + "(plaintext to be encoded/ or encoded string to be "
                    + "encoded)");
            System.exit(0);
        }

        int mode = Integer.parseInt(args[0]);
        String string = args[1];

        if (args[1] == null || args[1].equalsIgnoreCase("")) {
            System.out
                    .println("Invalid parameter, string to be "
                            + "encoded/decoded is empty.");
            System.exit(0);
        }

        Crypto crypto = new Crypto();

        if (mode == 0) {
            System.out.println("Encoded string: " + crypto.encrypt(string));
        } else if (mode == 1) {
            System.out.println("Decoded string: " + crypto.decode(string));
        } else {
            System.out
                    .println("Invalid flag provided, provide and 'e' or a 'd' "
                            + "flag");
        }
    }

}
