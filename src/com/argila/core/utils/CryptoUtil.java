package com.argila.core.utils;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * Class to perform encryption/decryption of strings.
 */
public final class CryptoUtil {
    /**
     * The linebreak variable.
     */
    private final byte[] linebreak = {};
    /**
     * The secret variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String secret;
    /**
     * The key variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private final SecretKey key;
    /**
     * The cipher variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private Cipher cipher;
    /**
     * The coder variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private Base64 coder;
    /**
     * The iv variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String iv;
    /**
     * The ivParameterSpec variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private IvParameterSpec ivParameterSpec;
    /**
     * The fileUtil Constant.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private FileUtil fileUtil;
    /**
     * The cryptoKeys Constant.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String[] cryptoKeys;
    /**
     * The props Constant.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private Props props;
    /**
     * The HOUR Constant.
     */
    @SuppressWarnings("unused")
    private final Logger logger;

    /**
     * @throws NoSuchAlgorithmException if anything goes wrong
     * @throws NoSuchProviderException if anything goes wrong
     * @throws NoSuchPaddingException if anything goes wrong
     * @param prop for properties object
     */
    public CryptoUtil(final Props prop)
            throws NoSuchAlgorithmException, NoSuchProviderException,
            NoSuchPaddingException {
        this.props = prop;
        this.logger = new Logger(getClass(), props);
        fileUtil = new FileUtil(this.props);
        cryptoKeys = fileUtil.readFromFile(props.getKeyFile());
        secret = cryptoKeys[0];
        iv = cryptoKeys[1];
        key = new SecretKeySpec(secret.getBytes(), "AES");
        cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
        coder = new Base64(32, linebreak, true);
        ivParameterSpec = new IvParameterSpec(iv.getBytes());
    }

    /**
     * @param plainText for text plain
     * @return string of ciphered text
     * @throws Exception if anything goes wrong
     */
    public String encrypt(final String plainText) throws Exception {
        cipher.init(Cipher.ENCRYPT_MODE, key, ivParameterSpec);
        byte[] cipherText = cipher.doFinal(plainText.getBytes());
        return bytesToHex(new String(coder.encode(cipherText)).getBytes());
    }

    /**
     * @param codedText for encrypted text
     * @return decrypted
     * @throws Exception if anything goes wrong
     */
    public String decrypt(final String codedText) throws Exception {
        byte[] encypted = coder.decode(hexToBytes(codedText));
        cipher.init(Cipher.DECRYPT_MODE, key, ivParameterSpec);
        byte[] decrypted = cipher.doFinal(encypted);
        return new String(decrypted);
    }

    /**
     * Converts a byte array to a hex string.
     *
     * @param data the byte array
     * @return the hex string
     */
    public static String bytesToHex(final byte[] data) {
        if (data == null || data.length == 0) {
            throw new IllegalArgumentException("Empty byte array");
        }

        int len = data.length;

        StringBuilder sb = new StringBuilder(2);
        for (int i = 0; i < len; i++) {
            if ((data[i] & 0xFF) < 16) {
                sb.append("0").append(Integer.toHexString(data[i] & 0xFF));
            } else {
                sb.append(Integer.toHexString(data[i] & 0xFF));
            }
        }

        return sb.toString();
    }

    /**
     * Converts a hex string to a byte array.
     *
     * @param str the hex string
     * @return the byte array
     */
    public static byte[] hexToBytes(final String str) {
        if (str == null) {
            throw new IllegalArgumentException("Empty string");
        } else if (str.length() < 2) {
            throw new IllegalArgumentException("Invalid hex string");
        } else {
            int len = str.length() / 2;
            byte[] buffer = new byte[len];

            for (int i = 0; i < len; i++) {
                buffer[i] = (byte) Integer.parseInt(
                        str.substring(i * 2, i * 2 + 2), 16);
            }

            return buffer;
        }
    }
}
