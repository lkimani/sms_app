/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.argila.core.utils;

/**
 *
 * @author amosl
 */
public final class Constants {

    /**
     * The ENTITY_STATE_ACTIVE Constant.
     */
    public static final int ENTITY_STATE_ACTIVE = 1;
    /**
     * The HOUR Constant.
     */
    public static final String HOUR = "HOUR";
    /**
     * The DAY Constant.
     */
    public static final String DAY = "DAY";
    /**
     * The MONTH Constant.
     */
    public static final String MONTH = "MONTH";
    /**
     * The MINUTE Constant.
     */
    public static final String MINUTE = "MINUTE";
    /**
     * The CELLULANT_CHARGE Constant.
     */
    public static final String CELLULANT_CHARGE = "cellulantCharge";
    /**
     * The INITIATOR_CHARGE Constant.
     */
    public static final String INITIATOR_CHARGE = "initiatorCharge";
    /**
     * The CELLULANT_COMMISSION Constant.
     */
    public static final String CELLULANT_COMMISSION = "cellulantCommission";
    /**
     * The INITIATOR_COMMISSION Constant.
     */
    public static final String INITIATOR_COMMISSION = "initiatorCommission";
    /**
     * The ACQUIRER_COMMISSION Constant.
     */
    public static final String ACQUIRER_COMMISSION = "acquirerCommission";
    /**
     * The STAT_CODE Constant.
     */
    public static final String STAT_CODE = "STAT_CODE";
    /**
     * The STAT_DESCRIPTION Constant.
     */
    public static final String STAT_DESCRIPTION = "STAT_DESCRIPTION";
    /**
     * The USERNAME Constant.
     */
    public static final String USERNAME = "USERNAME";
    /**
     * The PASSWORD Constant.
     */
    public static final String PASSWORD = "PASSWORD";
    /**
     * The SETTLEMENT_MODE Constant.
     */
    public static final String SETTLEMENT_MODE = "SETTLEMENT_MODE";
    /**
     * The SERVICE_IDS Constant.
     */
    public static final String SERVICE_IDS = "SERVICE_IDS";
    /**
     * The SETTLEMENT_ID Constant.
     */
    public static final String SETTLEMENT_ID = "settlementID";
    /**
     * The FILE_TYPE Constant.
     */
    public static final String FILE_TYPE = "fileType";
    /**
     * The PDF Constant.
     */
    public static final String PDF = "pdf";
    /**
     * The HOUR Constant.
     */
    public static final String EXCEL = "excel";

    /**
     * Constant used to identify requestLogs which do no have earnings matrix
     * configured for em.
     */
    public static final int NO_EARNINGS_MATRIX_CONFIGURED = 4;

    /**
     * Constant used to identify requestLogs which do no have commission matrix
     * configured for em.
     */
    public static final int NO_COMMISSION_MATRIX_CONFIGURED = 5;

    /**
     * Constant used when a settlement is initiated but a settlement log could
     * not be created.
     */
    public static final int SETTLEMENT_STATUS_FAILED_TO_LOG = -1;
    /**
     * Constant used when a settlement is initiated but a settlement log could
     * not be created.
     */
    public static final int UPDATE_RECON_FAILED = 103;

    /**
     * Constant used to indicate a scenario when settlement is initiated and
     * there are no payments to settle.
     */
    public static final int SETTLEMENT_STATUS_NOTHING_TO_SETTLE = -2;
    /**
     * Collection Report Settlement status.
     */
    public static final int SETTLEMENT_STATUS_COLLECTIONS_REPORT = 10;
    /**
     * Split Report settlement status.
     */
    public static final int SETTLEMENT_STATUS_SPLITS_REPORT = 11;
    /**
     * SQL Exception Settlement status.
     */
    public static final int SETTLEMENT_STATUS_SQL_EXCEPTION = 13;
    /**
     * IO Exception Settlement status.
     */
    public static final int SETTLEMENT_STATUS_IO_EXCEPTION = 14;
    /**
     * IO Exception Settlement status.
     */
    public static final int SETTLEMENT_EXCESS_REVERSAL_EXCEPTION = 15;
    public static final int SETTLEMENT_MISSING_CURRENCY = 16;
    public static final int SETTLEMENT_GENERIC_STATUS = 17;

    /**
     * Payment settlement type.
     */
    public static final String PAYMENT_SETTLEMENT_TYPE = "settlement";
    /**
     * Reversal settlement type.
     */
    public static final String REVERSAL_SETTLEMENT_TYPE = "reversal";
    /**
     * Reversal settlement type.
     */
    public static final String COMMISSIONS_SERVICE = "Commissions";
    
    public static final String RECEIPT_MESSAGE  = "CELLULANTKENYA";

    /**
     * Private constructor.
     */
    private Constants() {
        super();
    }

}
