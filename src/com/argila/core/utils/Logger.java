package com.argila.core.utils;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.LoggerFactory;

/**
 *
 * @author amosl
 *
 */
public final class Logger {

    /**
     * Logger object.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private org.slf4j.Logger logger;
    /**
     * Logger class constructor.
     * @param clazz class object
     * @param props properties object
     */
    public Logger(final Class<?> clazz, final Props props) {
        PropertyConfigurator.configure(props.getLoggerPropertiesFile());
        this.logger = LoggerFactory.getLogger(clazz);
    }

    /**
     * @return the logger
     */
    public org.slf4j.Logger getLogger() {
        return this.logger;
    }

    /**
     * @param arg0 argument
     * @param arg1 argument
     * @see org.slf4j.Logger#debug(String, Object)
     */
    public void debug(final String arg0, final Object arg1) {
        this.logger.debug(arg0, arg1);
    }

    /**
     * @param arg0 argument
     * @see org.slf4j.Logger#debug(String)
     */
    public void debug(final String arg0) {
        this.logger.debug(arg0);
    }

    /**
     * @param arg0 argument
     * @param arg1 argument
     * @see org.slf4j.Logger#error(String, Object)
     */
    public void error(final String arg0, final Object arg1) {
        this.logger.error(arg0, arg1);
    }

    /**
     * @param arg0 argument
     * @param arg1 argument
     * @see org.slf4j.Logger#error(String, Throwable)
     */
    public void error(final String arg0, final Throwable arg1) {
        this.logger.error(arg0, arg1);
    }

    /**
     * @param arg0 argument
     * @see org.slf4j.Logger#error(String)
     */
    public void error(final String arg0) {
        this.logger.error(arg0);
    }

    /**
     * @param arg0 argument
     * @param arg1 argument
     * @see org.slf4j.Logger#info(String, Object)
     */
    public void info(final String arg0, final Object arg1) {
        this.logger.info(arg0, arg1);
    }

    /**
     * @param arg0 argument
     * @see org.slf4j.Logger#info(String)
     */
    public void info(final String arg0) {
        this.logger.info(arg0);
    }

    /**
     * @param arg0 argument
     * @param arg1 argument
     * @see org.slf4j.Logger#warn(String, Object)
     */
    public void warn(final String arg0, final Object arg1) {
        this.logger.warn(arg0, arg1);
    }

    /**
     * @param arg0 argument
     * @param arg1 argument
     * @see org.slf4j.Logger#warn(String, Throwable)
     */
    public void warn(final String arg0, final Throwable arg1) {
        this.logger.warn(arg0, arg1);
    }

    /**
     * @param arg0 argument
     * @see org.slf4j.Logger#warn(String)
     */
    public void warn(final String arg0) {
        this.logger.warn(arg0);
    }

}
