package com.argila.core.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;

import javax.mail.MessagingException;

/**
 *
 * @author wekesa
 */
public final class FileUtil {

    /**
     * properties object.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private Props props;
    /**
     * Logger object.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private Logger logger;

    /**
     * FileUtil class constructor.
     *
     * @param prop for properties object
     */
    public FileUtil(final Props prop) {
        this.props = prop;
        this.logger = new Logger(getClass(), this.props);
    }

    /**
     * Method that reads data from a given file.
     *
     * @param fileName being read
     * @return records read
     */
    public String[] readFromFile(final String fileName) {
        String[] cryptoKeys = new String[2];
        File inFile = new File(fileName);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(
                new FileInputStream(inFile)));) {
            String line;
            int n = 0;
            while ((line = br.readLine()) != null && n < cryptoKeys.length) {
                cryptoKeys[n] = line;
                n++;
            }
        } catch (FileNotFoundException ex) {
            this.logger.error(ex.getMessage(), ex);
            return (null);
        } catch (IOException ex) {
            this.logger.error(ex.getMessage(), ex);
            return (null);
        }

        return cryptoKeys;
    }

    /**
     * This method is for updating Failed Queries File.
     *
     * @param file from where the queries are read
     * @param data from where the queries are read
     */
    public void updateFailedQueriesFile(final String file, final String data) {
        this.logger.info(CoreUtils.getLogPreString(this.getClass(),
                "updateFailedQueriesFile() - FailSafe procedure invoked...,"
                + " time: " + new java.util.Date().toString()));

        File queryFile = new File(file);
        this.logger.info("updateFailedQueriesFile() - Query access creation "
                + "and appending to file..., time "
                + new java.util.Date().toString());
        try {
            queryFile.createNewFile();

            writeToFile(file, data);
        } catch (IOException ex) {
            try {
                if (ex.getMessage().equals("There is not enough space "
                        + "on the disk")
                        || Files.getFileStore(FileSystems.getDefault().
                                getPath(this.props.
                                        getSettlementExcelFilesDir()))
                                .getUnallocatedSpace() < 50L) {
                    String message = "Hi, \r\n An error occured while a "
                            + "settlement process was in progress. The error"
                            + " is: " + ex;
                    new MailUtil(this.props, null).sendEmail(message, "");
                    System.exit(1);
                }
            } catch (IOException | MessagingException e) {
                this.logger.error(e.getMessage(), e);
            }
            this.logger.error(ex.getMessage(), ex);
        } catch (SecurityException ex) {
            this.logger.error(ex.getMessage(), ex);
        }
    }

    /**
     * This method writeToFile.
     *
     * @param filepath from where the queries are read
     * @param data from where the queries are read
     */
    private void writeToFile(final String filepath, final String data) {
        try (PrintWriter pout = new PrintWriter(new FileOutputStream(
                filepath, true));) {
            pout.println(data);

            this.logger.info("writeToFile() - Appended query: " + data + " to "
                    + "file: " + filepath);

        } catch (FileNotFoundException e) {
            this.logger.error(e.getMessage(), e);
            this.logger.error("writeToFile() - Failed to append query: " + data
                    + " to file: " + filepath);
        }
    }

} // end File_IO
