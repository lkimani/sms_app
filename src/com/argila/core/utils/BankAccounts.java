package com.argila.core.utils;

/**
 * Cellulant Bank account POJO
 *
 * @author Parfait Pascal <parfait.pascal@cellulant.com>
 */
public class BankAccounts {

    // Bank account country code
    private final String countryCode;

    // Settlment bank acount
    private final String account;

    // Settlment bank account name
    private final String accountName;

    // Settlement banck account branch code
    private final String branchCode;

    // Operation account.
    private final String operationAccount;

    // Operation account name
    private final String operationAccountName;

    // Operation bank account branch code
    private final String operationAccBranchCode;
    private final String postalAddress;

    public BankAccounts(String countryCode, String account, String accountName,
            String branchCode, String operationAccount,
            String operationAccountName, String operationAccBranchCode, String pAddress) {
        this.countryCode = countryCode;
        this.account = account;
        this.accountName = accountName;
        this.branchCode = branchCode;
        this.operationAccount = operationAccount;
        this.operationAccountName = operationAccountName;
        this.operationAccBranchCode = operationAccBranchCode;
        this.postalAddress = pAddress;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public String getAccount() {
        return account;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getOperationAccBranchCode() {
        return operationAccBranchCode;
    }

    public String getOperationAccountName() {
        return operationAccountName;
    }

    public String getOperationAccount() {
        return operationAccount;
    }
}
