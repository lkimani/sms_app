package com.argila.core.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.argila.core.db.MySQL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Transport;

/**
 * Created by lewis.kimani on 21/10/2017.
 */
public final class MailUtil {

    /**
     * properties object.
     */
    private final Props props;
    /**
     * properties object.
     */
    private final Logger logger;
    /**
     * properties object.
     */
    private final MySQL mySQL;

    /**
     * MailUtil Constructor.
     *
     * @param prop for properties object
     * @param mysql for mySQL connection object
     */
    public MailUtil(final Props prop, final MySQL mysql) {
        this.props = prop;
        this.logger = new Logger(getClass(), this.props);
        this.mySQL = mysql;
    }

    /**
     * Log an email in the emailQueue table with the status of the settlement
     * for the cases where a settlement log isn't successfully created or there
     * are no payments to settle.
     *
     * @param status Integer A flag to indicate the scenario(failed to log orN
     * no payments to settle)
     * @param attachment String Path to file to be attached to email
     * @param message
     * @param messageParams
     */
    public void sendSettlementStatusEmail(final int status,
            final String attachment, String message, String messageParams) {
        try {
            this.logger.info("sendSettlementStatusEmail - {");

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Dear Sir/Madam <br><br><p>A settlement process was "
                    + "initiated at ");
            stringBuilder.append(CoreUtils.getDateTime()).append(".");
            switch (status) {
                case Constants.SETTLEMENT_STATUS_FAILED_TO_LOG:
                    stringBuilder.append(" but the settlement log could "
                            + "not be created. Please contact support@cellulant.com  "
                            + "for more information.</p>");
                    break;

                case Constants.SETTLEMENT_STATUS_NOTHING_TO_SETTLE:
                    stringBuilder.append(
                            " There were not payments to be "
                            + "settled at " + "that moment, no settlement log"
                            + " created.");
                    break;
                case Constants.SETTLEMENT_MISSING_CURRENCY:
                    stringBuilder.append("There were missing configuration for "
                            + "cellulant  revenue with the following curreny(s) "
                            + " ").append(message).append(" ."
                            + " Merchants were settled succeccfully");
                    break;

                case Constants.SETTLEMENT_STATUS_SPLITS_REPORT:
                    stringBuilder.append("There were no annomalies to report. <br><br> "
                            + "Please find the attached report . </p>");
                    break;

                case Constants.SETTLEMENT_STATUS_SQL_EXCEPTION:
                    stringBuilder.append("A database error occured "
                            + "and settlement could not proceed.</p>");
                    break;
                case Constants.SETTLEMENT_STATUS_IO_EXCEPTION:
                    stringBuilder.append("The report could not be generated "
                            + "due to a file creation error. </p>");
                    break;
                case Constants.SETTLEMENT_EXCESS_REVERSAL_EXCEPTION:
                    stringBuilder.append("Settlement could not be done "
                            + " for the following  service(s) "
                            + "").append(message).append(" "
                            + " as reversal amount exeeded amount to be settled. </p>");
                    break;
                case Constants.SETTLEMENT_GENERIC_STATUS:
                    stringBuilder.append("There was no service set for auto settlements or settlement reports "
                            + " . No settlement log created </p>");
                    break;
                default:
                    stringBuilder.append(",Regards . </p>");

            }

            String query = "INSERT INTO emailQueue " + "(emailDestination, "
                    + "emailSubject, emailFrom, emailMessage, messageParams,templateID, "
                    + "emailAttachments) " + "VALUES (?,?,?,?,?,?,?)";
            sendEmail(stringBuilder.toString(), attachment);
            String[] settlementReportsRecipients
                    = this.props.getSettlementReportsRecipients();
            List<Object> params;
            DbUtil dbUtil = new DbUtil(this.props, this.mySQL);
            for (String settlementReportsRecipient
                    : settlementReportsRecipients) {
                params = new ArrayList<>();
                params.add(settlementReportsRecipient);
                params.add("Settlement File - " + CoreUtils.getDateTime());
                params.add(this.props.getSettlementNotificationSource());
                params.add(stringBuilder.toString());
                params.add(messageParams);
                params.add(1);
                params.add(attachment.isEmpty() ? null : attachment);

                //  dbUtil.updateRecord(query, params);
            }

            this.logger.info("sendSettlementStatusEmail - }");
        } catch (MessagingException ex) {
            this.logger.error("sendSettlementStatusEmail: Error" + ex.getMessage(), ex);
        }
    }

    /**
     * Send an email with two file attachments to the customer.
     *
     * @param msg to be send
     *
     * @throws AddressException if something goes wrong
     * @throws javax.mail.MessagingException if something goes wrong
     *
     */
    public void sendEmail(final String msg, String attachment) throws AddressException,
            MessagingException {
        this.logger.info("Enter sendEmail...");
        Properties emailProps = new Properties();
        emailProps.put("mail.transport.protocol", "smtp");
        emailProps.put("mail.smtp.host", this.props.getMailHost());
        emailProps.put("mail.smtp.socketFactory.port",
                this.props.getMailPort());
        emailProps.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        emailProps.put("mail.smtp.auth", this.props.getMailAuth());

        Session session = Session.getDefaultInstance(emailProps,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(props.getMailUsername(),
                        props.getMailPassword());
            }
        });

        session.setDebug(false);

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(this.props.getMailAccount()));
        // Now set the actual message
        final MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(msg.trim(), "text/html");
        // Create the message part
        BodyPart messageBodyPart = new MimeBodyPart();
        // Create a multipar message
        Multipart multipart = new MimeMultipart();

        // Set text message part
        multipart.addBodyPart(htmlPart);

        // Part two is attachment
        messageBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(attachment);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName("DAILY SETTLEMENT REPORT.xlsx");
        multipart.addBodyPart(messageBodyPart);
        Date javaUtilDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        message.setContent(multipart);
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(
                        this.props.getSystemAdminsEmails() + ","
                        + Arrays.toString(this.props.getSettlementReportsRecipients())));
        message.setSubject(this.props.getMailHeader()
                + " | " + formatter.format(javaUtilDate)
        );

        this.logger.info(" sendEmail() | Sending email now... ");

        Transport.send(message);
//         transport.close();
        this.logger.info(" sendEmail() | Email sent successfully... ");

        this.logger.info("Exit sendEmail...");
    }
}
