package com.argila.core.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author wekesa.
 */
public final class Props {

    /**
     * props object.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private Properties props;
    /**
     * processedStatus variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int processedStatus;
    /**
     * unprocessedStatus variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int unprocessedStatus;
    /**
     * settlementAccount variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String settlementAccount;

    /**
     * failedStatus variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int failedStatus;
    /**
     * Do not Settle Status.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int doNotSettleStatus;
    /**
     * retryStatus variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int retryStatus;
    /**
     * escalatedStatus variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int escalatedStatus;
    /**
     * maxSendRetries variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int maxSendRetries;
    /**
     * minRunID variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int minRunID;

    /**
     * settleToReportsStatus variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int settleToReportsStatus;
    /**
     * sleepTime variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int sleepTime;
    /**
     * sleepTimeMainThread variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int sleepTimeMainThread;
    /**
     * numOfChildren variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int numOfChildren;
    /**
     * dbPoolName variable.
     */
    @SuppressWarnings("FieldMayBeFinal")

    private String dbPoolName;
    /**
     * dbPoolSize variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int dbPoolSize;
    /**
     * dbUserName variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String dbUserName;
    /**
     * dbPassword variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String dbPassword;
    /**
     * dbHost variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String dbHost;
    /**
     * dbPort variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String dbPort;
    /**
     * dbName variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String dbName;
    /**
     * singleInstanceSocketPort variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int singleInstanceSocketPort;
    /**
     * loadErrors variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private List<String> loadErrors;
    /**
     * keyFile variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String keyFile;
    /**
     * settlementInProgressStatus variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int settlementInProgressStatus;
    /**
     * settlementExcelFilesDir variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String settlementExcelFilesDir;
    /**
     * settlementNotificationSource variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String settlementNotificationSource;
    /**
     * settlementReportsRecipients variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String[] settlementReportsRecipients;
    /**
     * loggerPropertiesFile variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String loggerPropertiesFile;
    /**
     * outputFilePath variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String outputFilePath;
    /**
     * Max insert size variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int maximumBatchInsertSize;
    /**
     * systemAdminsEmails variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String systemAdminsEmails;
    /**
     * defaultDate variable
     */
    private String defaultDate;
    /**
     * mailHost variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String mailHost;
    /**
     * mailPort variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private int mailPort;
    /**
     * mailAuth variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String mailAuth;
    /**
     * mailTls variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String mailTls;
    /**
     * mailUsername variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String mailUsername;
    /**
     * mailPassword variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String mailPassword;
    /**
     * mailAccount variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String mailAccount;
    /**
     * mailBody variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String mailBody;
    /**
     * processedStatus variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String mailHeader;
    /**
     * cellulantService variable.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private String inactiveSMSTemplate;
    /**
     * stanbicBranchCode variable.
     */

    /**
     * The properties file.
     */
    private static final String PROPS_FILE
            = "conf/SMSProcessorProperties.xml";
    /**
     * The SFTP properties file.
     */
    private static final String SFTP_PROPS_FILE = "conf/mail.properties";
    /**
     * Maximum allocated file size.
     */
    private Long maxAllowedAllocatedFileSize;

    /**
     * Props constructor.
     */
    @SuppressWarnings("Convert2Diamond")
    public Props() {
        this.loadErrors = new ArrayList<String>(0);
        loadProperties(PROPS_FILE);
        loadSftpProperties(SFTP_PROPS_FILE);

    }

    /**
     * loadSftpProperties file.
     *
     * @param sftpPropsFile for file name
     */
    private void loadSftpProperties(final String sftpPropsFile) {

        try {
            this.props.load(new FileInputStream(sftpPropsFile));

            this.mailHost = readStringProp("mail.smtp.host");
            this.mailPort = readIntegerProp("mail.smtp.mailport");
            this.mailAuth = readStringProp("mail.smtp.mailauth");
            this.mailTls = readStringProp("mail.smtp.mailtls");
            this.mailUsername = readStringProp("mail.smtp.username");
            this.mailPassword = readStringProp("mail.smtp.password");
            this.mailBody = readStringProp("mail.smtp.mailbody");
            this.mailHeader = readStringProp("mail.smtp.mailheader");
            this.mailAccount = readStringProp("mail.smtp.account");
        } catch (IOException e) {
            System.err.println("Exiting. Failed to load system properties: "
                    + e.getMessage());
        }

    }

    /**
     * @param propsFile for properties file.
     */
    private void loadProperties(final String propsFile) {
        try (InputStream propsStream = new FileInputStream(propsFile)) {
            this.props = new Properties();

            this.props.loadFromXML(propsStream);

            this.sleepTime = readIntegerProp("SLEEP_TIME");

            this.sleepTimeMainThread = readIntegerProp(
                    "SLEEP_TIME_MAIN_THREAD");

            this.numOfChildren = readIntegerProp("NUM_OF_CHILDREN");

            this.minRunID = readIntegerProp("MIN_RUN_ID");

            this.maxSendRetries = readIntegerProp("MAX_NUMBER_OF_SENDS");

            this.processedStatus = readIntegerProp("PROCESSED_STATUS");
            this.maximumBatchInsertSize = readIntegerProp("MAX_BATCH_INSERT_SIZE");

            this.unprocessedStatus = readIntegerProp("UNPROCESSED_STATUS");
            this.settlementAccount = readStringProp("SETTLEMENT_ACCOUNT");

            this.failedStatus = readIntegerProp("FAILED_STATUS");
            this.doNotSettleStatus = readIntegerProp("DO_NOT_SETTLE");

            this.retryStatus = readIntegerProp("RETRY_STATUS");

            this.inactiveSMSTemplate = readStringProp("INACTIVE_SMS_TEMPLATE");

            this.settleToReportsStatus
                    = readIntegerProp("SETTLE_TO_REPORTS_STATUS");

            this.escalatedStatus = readIntegerProp("ESCALATED_STATUS");

            this.inactiveSMSTemplate = readStringProp("INACTIVE_SMS_TEMPLATE");

            this.dbPoolName = readStringProp("DB_POOL_NAME");
            this.dbPoolSize = readIntegerProp("DB_POOL_SIZE");

            this.dbUserName = readStringProp("DB_USER_NAME");
            this.outputFilePath = readStringProp("OUTPUT_SQL_PATH");

            this.dbPassword = readStringProp("DB_PASSWORD");

            this.dbHost = readStringProp("DB_HOST");

            this.dbPort = readStringProp("DB_PORT");

            this.dbName = readStringProp("DB_NAME");

            this.keyFile = readStringProp("KEY_FILE");

            this.settlementInProgressStatus
                    = readIntegerProp("SETTLEMENT_IN_PROGRESS_STATUS");

            this.settlementExcelFilesDir
                    = readStringProp("SETTLEMENT_EXCEL_FILES_DIR");

            this.singleInstanceSocketPort
                    = readIntegerProp("SINGLE_INSTANCE_SOCKET_PORT");

            this.settlementNotificationSource
                    = readStringProp("SETTLEMENT_NOTIFICATION_SOURCE");

            this.settlementReportsRecipients
                    = readStringProp("SETTLEMENT_REPORTS_RECIPIENTS").
                            split(",");

            this.loggerPropertiesFile = "conf/log4j.properties";

            this.systemAdminsEmails = readStringProp("SYSTEM_ADMINS_EMAILS");
            this.defaultDate = readStringProp("DEFAULT_DATE");

            this.maxAllowedAllocatedFileSize
                    = readLongProp("MAX_ALLOWED_ALLOCATED_FILE_SIZE");

        } catch (FileNotFoundException ex) {
            System.err.println("Exiting. Could not find the properties file: "
                    + ex.getMessage());
        } catch (IOException ex) {
            System.err.println("Exiting. Failed to load system properties: "
                    + ex.getMessage());
        } catch (Exception ex) {
            System.err.println("Exiting. Serious errors occuring . "
                    + ex.getMessage());

        }
    }

    /**
     * @param key to hold the key.
     * @return propValue
     */
    public String readStringProp(final String key) {
        String propValue = this.props.getProperty(key);
        if (propValue == null) {
            this.loadErrors.add("ERROR ON : " + key + "   "
                    + "Value is not set or is missing. ");
        }

        return propValue;
    }

    /**
     * @param prop
     * @param key to hold the key.
     * @return propValue
     */
    public String readStringProp(final Properties prop, final String key) {
        String propValue = prop.getProperty(key);
        if (propValue == null) {
            this.loadErrors.add("ERROR ON : " + key + "   "
                    + "Value is not set or is missing. ");
        }

        return propValue;
    }

    /**
     *
     * @param key to hold the key.
     * @return propValue
     */
    public long readLongProp(final String key) {
        long propValue = 0L;
        String tmpHolder = this.props.getProperty(key, "");
        if (!(tmpHolder.isEmpty())) {
            try {
                propValue = Long.parseLong(tmpHolder);
            } catch (NumberFormatException ne) {
                this.loadErrors.add("ERROR ON : " + key + "   Value is not a "
                        + "double figure. " + ne.getMessage());
            }
        } else {
            this.loadErrors.add("ERROR ON : " + key + "   Value is not "
                    + "set or is missing. ");
        }

        return propValue;
    }

    /**
     * @param key to be returned.
     * @return prop value
     */
    public int readIntegerProp(final String key) {
        int propValue = 0;
        String tmpHolder = this.props.getProperty(key, "");
        if (!(tmpHolder.isEmpty())) {
            try {
                propValue = Integer.parseInt(tmpHolder);
            } catch (NumberFormatException ne) {
                this.loadErrors.add("ERROR ON : " + key + "   Value is not an "
                        + "integer figure. " + ne.getMessage());
            }

        } else {
            this.loadErrors.add("ERROR ON : " + key + "   Value is not set or "
                    + "is missing. ");
        }

        return propValue;
    }

    /**
     * @param key to be returned.
     * @return prop value
     */
    public double readDoubleProp(final String key) {
        double propValue = 0.0D;
        String tmpHolder = this.props.getProperty(key, "");
        if (!(tmpHolder.isEmpty())) {
            try {
                propValue = Double.parseDouble(tmpHolder);
            } catch (NumberFormatException ne) {
                this.loadErrors.add("ERROR ON : " + key + "   Value is not a "
                        + "double figure. " + ne.getMessage());
            }

        } else {
            this.loadErrors.add("ERROR ON : " + key + "   Value is not set or "
                    + "is missing. ");
        }

        return propValue;
    }

    /**
     * @param key to be returned.
     * @return prop value
     */
    public boolean readBooleanProp(final String key) {
        boolean booleanValue = false;
        String tmpHolder = this.props.getProperty(key);
        if (null != tmpHolder) {
            booleanValue = ("ON".equalsIgnoreCase(tmpHolder.trim()))
                    || ("1".equalsIgnoreCase(tmpHolder.trim()))
                    || ("TRUE".equalsIgnoreCase(tmpHolder.trim()));
        } else {
            this.loadErrors.add("ERROR ON : " + key + "   Value is not set "
                    + "or is missing. ");
        }

        return booleanValue;
    }

    /**
     * Load data from the file specified
     *
     * @param filePath The path of the file to extract data
     * @return Contents of the file passed
     */
    private String loadFileData(String filePath) {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }

            return sb.toString();
        } catch (Exception e) {

            return null;
        }
    }

    /**
     * @return prop value
     */
    public int getProcessedStatus() {
        return this.processedStatus;
    }

    /**
     * @return max batch insert value
     */
    public int getMaximumBatchInsertSize() {
        return this.maximumBatchInsertSize;
    }

    /**
     * @return unprocessedStatus
     */
    public int getUnprocessedStatus() {
        return this.unprocessedStatus;
    }

    /**
     * @return settlementAccount
     */
    public String getSettlementAccount() {
        return this.settlementAccount;
    }

    /**
     * @return failedStatus
     */
    public int getFailedStatus() {
        return this.failedStatus;
    }

    /**
     * @return failedStatus
     */
    public int getDoNoSettleStatus() {
        return this.doNotSettleStatus;
    }

    /**
     * @return the retryStatus
     */
    public int getRetryStatus() {
        return retryStatus;
    }

    /**
     * @return the escalatedStatus
     */
    public int getEscalatedStatus() {
        return escalatedStatus;
    }

    /**
     *
     * @return outputFilePath
     */
    public String getOutputSqlPath() {
        return outputFilePath;
    }

    /**
     * @return the settleToReportsStatus
     */
    public int getSettleToReportsStatus() {
        return settleToReportsStatus;
    }

    /**
     * @return the maxSendRetries
     */
    public int getMaxSendRetries() {
        return this.maxSendRetries;
    }

    /**
     * @return the minRunID.
     */
    public int getMinRunID() {
        return this.minRunID;
    }

    /**
     * @return the sleepTime
     */
    public int getSleepTime() {
        return this.sleepTime;
    }

    /**
     * @return the numOfChildren
     */
    public int getNumOfChildren() {
        return this.numOfChildren;
    }

    /**
     * @return the dbPoolName
     */
    public String getDbPoolName() {
        return this.dbPoolName;
    }

    /**
     * @return the dbPoolSize
     */
    public int getDbPoolSize() {
        return this.dbPoolSize;
    }

    /**
     * @return the dbUserName
     */
    public String getDbUserName() {
        return this.dbUserName;
    }

    /**
     * @return the dbPassword
     */
    public String getDbPassword() {
        return this.dbPassword;
    }

    /**
     * @return the dbHost
     */
    public String getDbHost() {
        return this.dbHost;
    }

    /**
     * @return the dbPort
     */
    public String getDbPort() {
        return this.dbPort;
    }

    /**
     * @return the dbName
     */
    public String getDbName() {
        return this.dbName;
    }

    /**
     * @return the loadErrors
     */
    public List<String> getLoadErrors() {
        return Collections.unmodifiableList(this.loadErrors);
    }

    /**
     * @return the keyFile
     */
    public String getKeyFile() {
        return keyFile;
    }

    /**
     * @return the settlementInProgressStatus
     */
    public int getSettlementInProgressStatus() {
        return settlementInProgressStatus;
    }

    /**
     * @return the settlementExcelFilesDir
     */
    public String getSettlementExcelFilesDir() {
        return settlementExcelFilesDir;
    }

    /**
     * @return the singleInstanceSocketPort
     */
    public int getSingleInstanceSocketPort() {
        return singleInstanceSocketPort;
    }

    /**
     * @return the settlementNotificationSource
     */
    public String getSettlementNotificationSource() {
        return settlementNotificationSource;
    }

    /**
     * @return the settlementReportsRecipients
     */
    public String[] getSettlementReportsRecipients() {
        return settlementReportsRecipients;
    }

    /**
     * @return the loggerPropertiesFile
     */
    public String getLoggerPropertiesFile() {
        return this.loggerPropertiesFile;
    }

    /**
     * @return the systemAdminsEmails
     */
    public String getSystemAdminsEmails() {
        return systemAdminsEmails;
    }

    /**
     * @return the defaultDate
     */
    public String getDefaultDate() {
        return defaultDate;
    }

    /**
     * @return the mailHost
     */
    public String getMailHost() {
        return mailHost;
    }

    /**
     * @return the mailPort
     */
    public int getMailPort() {
        return mailPort;
    }

    /**
     * @return the mailAuth
     */
    public String getMailAuth() {
        return mailAuth;
    }

    /**
     * @return the mailTls
     */
    public String getMailTls() {
        return mailTls;
    }

    /**
     * @return the mailUsername
     */
    public String getMailUsername() {
        return mailUsername;
    }

    /**
     * @return the mailPassword
     */
    public String getMailPassword() {
        return mailPassword;
    }

    /**
     * @return the mailAccount
     */
    public String getMailAccount() {
        return mailAccount;
    }

    /**
     * @return the mailBody
     */
    public String getMailBody() {
        return mailBody;
    }

    /**
     * @return the mailHeader
     */
    public String getMailHeader() {
        return mailHeader;
    }

    /**
     * @return the inactiveSMSTemplate
     */
    public String getInactiveSMSTemplate() {
        return inactiveSMSTemplate;
    }

    /**
     * @return the sleepTime for main Thread
     */
    public int getSleepTimeMainThread() {
        return sleepTimeMainThread;
    }

    /**
     * Maximum allowed allocated file size
     *
     * @return maxAllowedAllocatedFileSize
     */
    public Long getMaxAllowedAllocatedFileSize() {
        return maxAllowedAllocatedFileSize;
    }

}
