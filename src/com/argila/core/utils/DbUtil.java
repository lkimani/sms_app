/**
 *
 */
package com.argila.core.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import com.argila.core.db.MySQL;

/**
 * @author sunz
 *
 */
public final class DbUtil {
    /**
     * The properties object.
     */
    private final Props props;
    /**
     * The logging object.
     */
    private final Logger logger;
    /**
     * The mySQL connection object.
     */
    private final MySQL mySQL;
    /**
     * The fileUtil object.
     */
    @SuppressWarnings("FieldMayBeFinal")
    private FileUtil fileUtil;

    /**
     *DbUtil constructor.
     * @param prop for properties object
     * @param mysql for mySQL connection object
     */
    public DbUtil(final Props prop, final MySQL mysql) {
        this.props = prop;
        this.logger = new Logger(getClass(), this.props);
        this.mySQL = mysql;
        this.fileUtil = new FileUtil(this.props);
    }
    /**
     * The fileUtil object.
     * @param  updateQuery for update query
     * @param params parameters passed
     * @return status of execution
     */
    public int updateRecord(final String updateQuery,
            final List<Object> params) {
        return (int) this.updateRecord(updateQuery, params, false);
    }
    /**
     * The updateRecord method.
     * @param  updateQuery for update query
     * @param params parameters passed
     * @param returnGeneratedKeys boolean variable
     * @return status of execution
     */
    public long updateRecord(final String updateQuery,
            final List<Object> params,
            final boolean returnGeneratedKeys) {
        this.logger.info("Enter updateRecord-- <<<");

        long lresult = 0;
        int result = 0;
        try (Connection conn = this.mySQL.getConnection();
                PreparedStatement stmt = returnGeneratedKeys
                        ? conn.prepareStatement(updateQuery,
                                PreparedStatement.RETURN_GENERATED_KEYS)
                        : conn.prepareStatement(updateQuery);) {
            String query = prepareRowQueryFromPreparedPayload(updateQuery,
                    params);
            this.logger.debug("updateRecord-- query: " + query);

            int counter = 0;
            for (Object param : params) {
                ++counter;

                if (param instanceof Integer) {
                    stmt.setInt(counter, ((Integer) param));
                } else if (param instanceof String) {
                    stmt.setString(counter, (String) param);
                } else if (param instanceof Float) {
                    stmt.setFloat(counter, ((Float) param));
                } else if (param instanceof java.util.Date) {
                    if (param instanceof java.sql.Timestamp) {
                        stmt.setTimestamp(counter, (java.sql.Timestamp) param);
                    } else {
                        stmt.setDate(counter, (java.sql.Date) param);
                    }
                } else if (param instanceof Boolean) {
                    stmt.setBoolean(counter, ((Boolean) param));
                } else if (param instanceof Long) {
                    stmt.setLong(counter, ((Long) param));
                } else if (param instanceof Double) {
                    stmt.setDouble(counter, ((Double) param));
                } else {
                    stmt.setString(counter, (String) param);
                }
            }

            result = stmt.executeUpdate();

            if (returnGeneratedKeys) {
                try (ResultSet rs = stmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        lresult = rs.getLong(1);
                    }
                }
            } else {
                lresult = result;
            }

        } catch (SQLException e) {
            this.logger.error(e.getMessage(), e);
            this.logger.info("updateRecord() - Invoking failsafe => "
                    + "updateFile()");

            String query = prepareRowQueryFromPreparedPayload(updateQuery,
                    params);
            this.fileUtil.updateFailedQueriesFile("FAILED_QUERIES.TXT", query);
        } catch (Exception e) {
            this.logger.error(e.getMessage(), e);
            this.logger.info("updateRecord() - Invoking failsafe =>"
                    + " updateFile()");

            String query = prepareRowQueryFromPreparedPayload(updateQuery,
                    params);
            this.fileUtil.updateFailedQueriesFile("FAILED_QUERIES.TXT", query);
        }

        this.logger.info("Exit updateRecord-- >>>, result: " + result);
        return lresult;
    }
    /**
     * The prepareRowQueryFromPreparedPayload function.
     * @param  updateQuery for update query
     * @param params parameters passed
     * @return status of execution
     */
    public String prepareRowQueryFromPreparedPayload(final String updateQuery,
            final List<Object> params) {

        String query = updateQuery.replaceAll("[?]", "'%s'");
        query = String.format(query.replace("\\n", ""), params.toArray());

        return query;
    }

}
