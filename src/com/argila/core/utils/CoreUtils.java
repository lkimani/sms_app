package com.argila.core.utils;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author sunz
 * @since 2014-06-24
 */
public final class CoreUtils {

    /**
     * Return a formatted date -- now!
     *
     * @return String Formatted date
     */
    public static String getDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new java.util.Date());
    }

    /**
     *
     * @param tCount
     * @param sCount
     * @param commissions
     * @param tAmout
     * @return String Json Array
     */
    public static String getMessageParams(int tCount, int sCount, int commissions, int tAmout) {
        JSONObject messageJSON = new JSONObject();
        try {

            messageJSON.put("TRANSACTIONSCOUNT", tCount);
            messageJSON.put("MESSAGE", "");
            messageJSON.put("NAME", "");
            messageJSON.put("SERVICESCOUNT", sCount);
            messageJSON.put("TOTALAMOUNT", tAmout);
            messageJSON.put("TOTALCOMMISSIONS", commissions);

        } catch (JSONException ex) {
            Logger.getLogger(CoreUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return messageJSON.toString();
    }

    /**
     * Return a formatted date -- now!
     *
     * @param aClass class to be received
     * @param s string to be received
     * @return String Formatted date
     */
    public static String getLogPreString(final Class<?> aClass,
            final String s) {
        return "";
    }
//
//    public static void encrypt() {
//
//        FileInputStream key = null;
//        try {
//            Security.addProvider(new BouncyCastleProvider());
//            //Load Public Key File
//            key = new FileInputStream("res/keys/public.bpg");
//            PGPPublicKey pubKey = KeyBasedFileProcessor.readPublicKey(key);
//            //Output file
//            FileOutputStream out = new FileOutputStream("target/enc.bpg");
//            //Input file
//            String inputFilename = "src/main/resources/plaintext.txt";
//            //Other settings
//            boolean armor = false;
//            boolean integrityCheck = false;
//            KeyBasedFileProcessor.encryptFile(out, inputFilename,
//                    pubKey, armor, integrityCheck);
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(CoreUtils.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(CoreUtils.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (NoSuchProviderException ex) {
//            Logger.getLogger(CoreUtils.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                key.close();
//            } catch (IOException ex) {
//                Logger.getLogger(CoreUtils.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }

    /**
     * Private constructor.
     */
    private CoreUtils() {
        super();
    }
}
